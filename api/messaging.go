package api

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/issuance-service/entity"
	"sync"
	"time"
)

type urlBuildOffer struct {
	CredentialType string `json:"credentialType"`
	GrantType      string `json:"grantType"`
	TwoFactor      struct {
		Enabled          bool   `json:"enabled"`
		RecipientType    string `json:"recipientType"`
		RecipientAddress string `json:"recipientAddress"`
	} `json:"twoFactor"`
	TenantId string `json:"tenantId"`
	Host     string `json:"host"`
}

func (u *urlBuildOffer) validate() error {
	if u.CredentialType == "" {

		return fmt.Errorf("credentialType not set")
	}
	if u.GrantType == "" {

		return fmt.Errorf("grantType not set")
	}
	if u.TenantId == "" {

		return fmt.Errorf("tenantId not set")
	}
	if u.Host == "" {

		return fmt.Errorf("host not set")
	}

	return nil
}

type preAuthReply struct {
	AuthCode string `json:"authCode"`
}

const supportedGrantType = "urn:ietf:params:oauth:grant-type:pre-authorized_code"

var preAuthClient *cloudeventprovider.CloudEventProviderClient

func offerHandler(ctx context.Context, event event.Event) (*event.Event, error) {
	var currentOffer urlBuildOffer

	err := json.Unmarshal(event.Data(), &currentOffer)
	if err != nil {
		log.Error(err, "could not unmarshal offer")

		return nil, err
	}

	err = currentOffer.validate()
	if err != nil {
		log.Error(err, "currentOffer not valid")

		return nil, err
	}

	if currentOffer.GrantType != supportedGrantType {
		err = fmt.Errorf("grantType '%s' is not supported", currentOffer.GrantType)
		log.Error(err, "could not proceed with offer")

		return nil, err
	}

	issuer, err := wellKnownApi.CredentialIssuer(ctx, currentOffer.CredentialType, currentOffer.TenantId)
	if err != nil {
		log.Error(err, "could not get credential issuer")

		return nil, err
	}

	preAuthRequestData, err := json.Marshal(currentOffer.TwoFactor)
	if err != nil {
		log.Error(err, "could not marshal preAuthRequestData")

		return nil, err
	}
	preAuthRequestEvent, err := cloudeventprovider.NewEvent("issuance/service", "pre.auth.request.v1", preAuthRequestData)
	if err != nil {
		log.Error(err, "could not create preAuthRequestEvent")

		return nil, err
	}
	preAuthReplyEvent, err := preAuthClient.Request(preAuthRequestEvent, 60*time.Second)

	var preAuthReplyData preAuthReply
	if err = json.Unmarshal(preAuthReplyEvent.Data(), &preAuthReplyData); err != nil {
		log.Error(err, "could not unmarshal preAuthRequestReply")

		return nil, err
	}

	offerObject := entity.NewCredentialOfferObject(
		issuer,
		currentOffer.CredentialType,
		preAuthReplyData.AuthCode,
		currentOffer.TwoFactor.Enabled,
	)

	url, err := entity.NewCredentialOfferUrl(currentOffer.Host, currentOffer.TenantId, offerObject)
	if err != nil {
		log.Error(err, "could not create credentialOfferUrl")

		return nil, err
	}

	offerReplyData, err := json.Marshal(url)
	if err != nil {
		log.Error(err, "could not marshal credentialOfferUrl to offerReplyData")

		return nil, err
	}
	offerReplyEvent, err := cloudeventprovider.NewEvent("issuance/service", "credential.offer.url.v1", offerReplyData)
	if err != nil {
		log.Error(err, "could not create offerReplyEvent")

		return nil, err
	}

	return &offerReplyEvent, nil
}

func startMessaging(offerTopic string, preAuthTopic string, wg *sync.WaitGroup) {
	defer wg.Done()

	offerClient, err := cloudeventprovider.NewClient(cloudeventprovider.Rep, offerTopic)
	if err != nil {
		return
	}

	preAuthClient, err = cloudeventprovider.NewClient(cloudeventprovider.Req, preAuthTopic)
	if err != nil {
		return
	}

	if err := offerClient.Reply(offerHandler); err != nil {
		return
	}
}
