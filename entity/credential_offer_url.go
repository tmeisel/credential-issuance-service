package entity

import (
	"encoding/json"
	"fmt"
	"net/url"
)

type CredentialOfferUrl struct {
	CredentialOfferUrl string `json:"credentialOfferUrl"`
}

func NewCredentialOfferUrl(host string, tenantId string, object *CredentialOfferObject) (*CredentialOfferUrl, error) {
	marshal, err := json.Marshal(object)
	if err != nil {
		return nil, fmt.Errorf("could not marshal credentialOfferObject: %w", err)
	}
	escape := url.QueryEscape(string(marshal))

	return &CredentialOfferUrl{
		CredentialOfferUrl: fmt.Sprintf("https://%s/%s/credential?credentialoffer=%s", host, tenantId, escape),
	}, nil
}
