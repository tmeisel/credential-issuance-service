package well_known

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/issuance-service/entity"
	"time"
)

type wellKnownRequest struct {
	TenantId string `json:"tenantId"`
}

type WellKnown struct {
	reqClient *cloudeventprovider.CloudEventProviderClient
	log       *logr.Logger
}

func New(reqClient *cloudeventprovider.CloudEventProviderClient, logger *logr.Logger) (*WellKnown, error) {
	return &WellKnown{reqClient: reqClient, log: logger}, nil
}

func (wk *WellKnown) CredentialIssuer(ctx context.Context, credentialType string, tenantId string) (string, error) {
	information, err := wk.requestWellKnownInformation(ctx, tenantId)
	if err != nil {

		return "", fmt.Errorf("could not get wellknown information: %w", err)
	}

	for _, issuer := range information {
		for _, credential := range issuer.CredentialsSupported {
			if credential.ID == credentialType {

				return issuer.CredentialIssuer, nil
			}
		}
	}

	return "", fmt.Errorf("no issuer for credentialType '%s' and tenantId '%s': credentialType not supported", credentialType, tenantId)
}

func (wk *WellKnown) Close() {
	if err := wk.reqClient.Close(); err != nil {
		wk.log.Error(err, "could not close reqClient")
	}
}

func (wk *WellKnown) requestWellKnownInformation(ctx context.Context, tenantId string) ([]entity.CredentialIssuer, error) {
	select {
	case <-ctx.Done():

		return nil, fmt.Errorf("context already closed")
	default:
		requestData, err := json.Marshal(wellKnownRequest{TenantId: tenantId})
		if err != nil {

			return nil, fmt.Errorf("could not create requestData: %w", err)
		}
		requestEvent, err := cloudeventprovider.NewEvent("issuance.service", "wellknown.request.v1", requestData)
		if err != nil {

			return nil, fmt.Errorf("could not create requestEvent: %w", err)
		}

		res, err := wk.reqClient.Request(requestEvent, time.Second*10)
		if err != nil {

			return nil, fmt.Errorf("could not request wellknown information: %w", err)
		}

		var credentialIssuer []entity.CredentialIssuer
		if err := json.Unmarshal(res.Data(), &credentialIssuer); err != nil {

			return nil, fmt.Errorf("could not unmarshal wellknown response '%v': %w", res.Data(), err)
		}

		return credentialIssuer, nil
	}
}
